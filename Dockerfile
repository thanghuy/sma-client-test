# Use an official Node.js runtime as the base image
FROM node:lts

# Set the working directory in the container
WORKDIR /app

# Copy the package.json and package-lock.json to the working directory
COPY package*.json ./

# Install project dependencies
RUN npm install

# Copy the entire project to the working directory
COPY . .

# Build the React.js application
RUN npm run build

# Install Serve globally
RUN npm install -g serve

# Expose port 3000 (default React.js development server port)
EXPOSE 3000

# Start Serve to serve the built React.js application
CMD ["serve", "-s", "build"]