import React from "react";
import RouterRoot from "./router/router";
import 'antd/dist/reset.css';
import './index.css';

const App = () => {
  return <RouterRoot />;
};

export default App;
