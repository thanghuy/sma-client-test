import React, { useState, useEffect } from "react";
import "./style.css";
import Intro from "@assets/images/intro.svg";
import { Button, Input, Progress } from "antd";
import Test from "./components/Test";
import { Voucher } from "@src/test";

const Home: React.FC = () => {
  const [percentLoading, setPercentLoading] = useState<number>(0);
  useEffect(() => {
    const delayLoading = setInterval(() => {
      if (percentLoading < 100) {
        setPercentLoading(percentLoading + 40);
      } else {
        setPercentLoading(100);
      }
    }, 400);
    if (percentLoading == 100) {
      clearInterval(delayLoading);
    }
    return () => {
      clearInterval(delayLoading);
    };
  }, [percentLoading]);
  return (
    <div className="onboarding-home">
      <div className="image-intro">
        <img src={Intro} alt="err" width={300} />
      </div>
      {percentLoading == 100 ? (
        <div className="user-input-email">
          <Input
            size="large"
            style={{ width: "500px" }}
            placeholder="Nhập địa chỉ email"
          />
          <Button size="large" type="primary">
            Đăng nhập bb
          </Button>
        </div>
      ) : (
        <div className="loading-onboarding">
          <Progress percent={percentLoading} />
        </div>
      )}
      <Test />
      <Voucher />
      <Progress percent={30} />
      <Progress percent={50} status="active" />
      <Progress percent={70} status="exception" />
      <Progress percent={100} />
      <Progress percent={50} showInfo={false} />
    </div>
  );
};
export default Home;
