//import * as classnames from 'classnames';
import React from 'react';
//import Button from '@haravan/react-components/lib/Button';

import './index.css';

interface IVoucherProps {
    isExpired?: boolean
}

// eslint-disable-next-line react/display-name
export const Voucher = React.memo((props: IVoucherProps) => {
    const { isExpired } = props;

    // const clsContainer = classnames('hsl-voucher-card', {
    //     'expired': isExpired
    // })

    return <div>
        <div className='voucher-left'>
            <div className="voucher-content">
                <div className="voucher-value">145,000đ</div>
                <div className="voucher-title voucher-text-ellipsis" title=''>Đơn tối thiểu 200K</div>
                <div className="voucher-subtitle voucher-text-ellipsis" title=''>Có giá trị đến 26/09/2022</div>
            </div>
        </div>
        <div className='voucher-right'>
            <div className='voucher-content'>
                {isExpired
                    ? <img src="https://static.harasocial.com/web/voucher_expired.svg" alt="" />
                    : <button className='voucher-btn'>Sử dụng</button>}
            </div>
        </div>
    </div>
})